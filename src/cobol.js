import { CobolLexer } from "./lexer";
import { parserInstance } from "./parser";

export function parseCobol(text) {
    const lexResult = CobolLexer.tokenize(text);

    // setting a new input will RESET the parser instance's state.
    parserInstance.input = lexResult.tokens;

    // any top level rule may be used as an entry point
    const cst = parserInstance.program();
    //parserInstance.MoveStatement();

    return {
        cst: cst,
        lexErrors: lexResult.errors,
        parseErrors: parser.errors,
    };
}