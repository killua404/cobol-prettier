import {
    CobolLexer,
    cobolTokens,
    Identifier,
    StringLiteralDouble,
    StringLiteralSingle,

    MOVE,
    TO,
    TITLE
} from "./lexer.js";
import { CstParser } from "chevrotain";

export class CobolParser extends CstParser {
    constructor() {
        super(cobolTokens);

        const $ = this;

        $.RULE("String", () => {
            $.OR([
                {ALT: () => { $.CONSUME1(StringLiteralDouble)}},
                {ALT: () => { $.CONSUME2(StringLiteralSingle)}}
            ])
        })

        $.RULE("Title", ()=> {
            $.CONSUME1(TITLE);
            $.SUBRULE($.String);
        })

        // PROCEDURE DIVISION
        $.RULE("MoveStatement", () => {
            $.CONSUME1(MOVE);
            $.OR([
                { ALT: () => $.SUBRULE($.String)},
                { ALT: () => $.CONSUME7(Identifier) },
            ]);
            $.CONSUME2(TO);
            $.CONSUME3(Identifier);
        })
        this.performSelfAnalysis();
    }
}

export const parserInstance = new CobolParser();
