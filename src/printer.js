import { BaseTomlCstVisitor } from "./parser.js";
import { tokensDictionary as t } from "./lexer.js";
import { doc, version } from "prettier";

const { group, indent, join, line, softline } = doc.builders;

function print(path, options, print) {
    const node = path.getValue();

    if (Array.isArray(node)) {
        return concat(path.map(print));
      }
    
      switch (node.type) {
        case 'Integer':
            return node.value.toString();

        default:
          return '';
      }
  }
  
  export { print };