import { lex } from "./lexer.js";
import fs from "fs";

//const inputText = "MOVE 'TEST' TO W-INPUT";

const inputText = fs.readFileSync("./tests/data/BASICS.cbl", 'utf8');

console.log(inputText);

const lexingResult = lex(inputText);
let jsonResult = JSON.stringify(lexingResult, null, "\t");
//console.log(jsonResult);

fs.writeFileSync("./tests/out/BASICS.json", jsonResult);