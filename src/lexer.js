import { createToken, Lexer } from "chevrotain";

// KEY WORDS

// GENERAL

export const Identifier = createToken({
    name: "Identifier",
    pattern: /[a-zA-Z][a-zA-Z-]*/,
});

export const StringLiteralDouble = createToken({
    name: "StringLiteral",
    pattern: /"(?:[^\\"]|\\(?:[bfnrtv"\\/]|u[0-9a-fA-F]{4}))*"/,
});
export const StringLiteralSingle = createToken({
    name: "StringLiteral",
    pattern: /'(?:[^\\"]|\\(?:[bfnrtv"\\/]|u[0-9a-fA-F]{4}))*'/,
});

export const Integer = createToken({ name: "Integer", pattern: /0|[1-9]\d*/ });

// SYMBOLS
export const LBracket = createToken({ name: "LBracket", pattern: /\(/ });
export const RBracket = createToken({ name: "RBracket", pattern: /\)/ });
export const Comma = createToken({ name: "Comma", pattern: /,/ });
export const Colon = createToken({ name: "Colon", pattern: /:/ });

export const GreaterThan = createToken({ name: "GreaterThan", pattern: />/ });
export const LessThan = createToken({ name: "LessThan", pattern: /</ });

export const Plus = createToken({ name: "Plus", pattern: /\+/ });
export const Minus = createToken({ name: "Minus", pattern: /-/ });
export const Mult = createToken({ name: "Mult", pattern: /\*/ });
export const Div = createToken({ name: "Div", pattern: /\// });

export const WhiteSpace = createToken({
    name: "WhiteSpace",
    pattern: /\s+/,
    group: Lexer.SKIPPED,
});

export const TITLE = createToken({
    name: "TITLE",
    pattern: /TITLE/,
    longer_alt: Identifier,
});

export const ALL = createToken({
    name: "ALL",
    pattern: /ALL/,
    longer_alt: Identifier,
});

// PROCEDURE DIVISION

export const MOVE = createToken({
    name: "MOVE",
    pattern: /MOVE/,
    longer_alt: Identifier,
});

export const TO = createToken({
    name: "TO",
    pattern: /TO/,
    longer_alt: Identifier,
});

export const ACCEPT = createToken({
    name: "ACCEPT",
    pattern: /ACCEPT/,
    longer_alt: Identifier,
});

export const ADD = createToken({
    name: "ADD",
    pattern: /ADD/,
    longer_alt: Identifier,
});

export const AND = createToken({
    name: "AND",
    pattern: /AND/,
    longer_alt: Identifier,
});

export const cobolTokens = [
    WhiteSpace,
    StringLiteralDouble,
    StringLiteralSingle,
    // "keywords" appear before the Identifier

    TITLE,
    ALL,

    // PROCEDURE
    MOVE,
    TO,
    ACCEPT,
    ADD,
    AND,
    Comma,
    // The Identifier must appear after the keywords because all keywords are valid identifiers.
    Identifier,
    LBracket,
    RBracket,
    Integer,
    GreaterThan,
    LessThan,
];

export const CobolLexer = new Lexer(cobolTokens);

LBracket.LABEL = "'('";
RBracket.LABEL = "')'";
Comma.LABEL = "','";
Colon.LABEL = "':'";

export function lex(inputText) {
    const lexingResult = CobolLexer.tokenize(inputText);
    //console.log(lexingResult);
    if (lexingResult.errors.length > 0) {
        throw Error("Sad Sad Panda, lexing errors detected");
    }

    return lexingResult;
}