import { locStart, locEnd } from "./loc.js";
import { parse } from "./parser.js";
import { print } from "./printer.js";

// https://prettier.io/docs/en/plugins.html#languages
const languages = [
  {
    extensions: [".cbl",".srb",".sre"],
    name: "COBOL",
    parsers: ["cobol"]
  },
];

// https://prettier.io/docs/en/plugins.html#parsers
const parsers = {
  cobol: {
    astFormat: "cobol-cst",
    parse: (text, parsers, options) => parse(text),
    locStart,
    locEnd,
  },
};

// https://prettier.io/docs/en/plugins.html#printers
const printers = {
  "cobol-cst": {
    print,
  },
};

export {
  languages,
  parsers,
  printers,
  // TODO: are any options/default options needed?
  //  - Prefer certain inline variants when possible?
  //  - Indent nested props?
};