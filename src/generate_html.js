import path, { dirname } from "path";
import fs from "fs";
import { fileURLToPath } from "url";
import { createSyntaxDiagramsCode } from "chevrotain";
import { CobolParser } from "./parser.js";

const __dirname = dirname(fileURLToPath(import.meta.url));

// extract the serialized grammar.
const parserInstance = new CobolParser();
const serializedGrammar = parserInstance.getSerializedGastProductions();

// create the HTML Text
const htmlText = createSyntaxDiagramsCode(serializedGrammar);

// Write the HTML file to disk
const outPath = path.resolve(__dirname, "./");
fs.writeFileSync(outPath + "/generated_diagrams.html", htmlText);