import assert from "assert";
import { parseCobol } from "./cobol";

describe("The COBOL Grammar", () => {
    it("can parse a simple COBOL sample without errors", () => {
        const inputText =
            "MOVE 'TEST' TO W-INPUT ";
        const lexAndParseResult = parseCobol(inputText);

        assert.equal(lexAndParseResult.lexErrors.length, 0);
        assert.equal(lexAndParseResult.parseErrors.length, 0);
    });
});